# braintree-react

## Getting Started
```
git clone https://gitlab.com/braintree-payment/braintree-react.git
cd braintree-react
npm i
npm start
```
## How to fill the payment form
All fields are required

### Order part
- Name: string (max lenght 40)
- Prefix: drop down
- Phone: string (max Length 20)
- Currency: drop down
- Price: float (max length 60)

### Payment part
- Card holder name: string (max length 40)
- Card number: 
    - format: XXXX-XXXX-XXX-XXXX
    - min length: 14 (17 include '-') int
    - masked field
- Expiration date: 
    - drop down
    - past date disabled
- CCV:
    - min length: 3 int
    - max length: 4 int

## Paypal Sandbox Accounts
buyer
```
username: lunlun414-buyer@gmail.com
password: iambuyer

username: buyer-02@gmail.com
password: iambuyer
```
facilitator
```
username: lunlun414-facilitator@gmail.com
password: iamseller
```

## References
* [react-create-app](https://github.com/facebookincubator/create-react-app)


