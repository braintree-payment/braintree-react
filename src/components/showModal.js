import React from 'react'
import { Modal } from 'antd'
export const showModal = (res) => {

  const { status } = res
  const { pathname } = window.location
  let isError = status === 200 ? false : true

  if (isError) {
    if(res.response !== undefined){
      const { response } = res
      Modal.error({
        title: response.name || `${response.status} ${response.statusText}`,
        content: (
          <div>
            <p>{response.data.message || response.data.response.message}</p>
          </div>
        )
      })
    } else {
      Modal.error({
        title: res.name,
        content: (
          <div>
            <p>{res.message}</p>
          </div>
        )
      })
    }
  
  } else {

    if (pathname === '/') {

      Modal.success({
        title: 'Payment success',
        content: `Payment reference code: ${res.data.id || res.data}`
      })

    } else if (pathname === '/record') {

      const { id, currency, amount, payer, phone } = res.data
      Modal.info({
        title: `Payment ID: ${id}`,
        content: (
          <div>
            <p>Name: {payer}</p>
            <p>Phone: {phone}</p>
            <p>Amount: {`$${currency} ${amount}`}</p>
          </div>
        )
      })
    }
  }
}