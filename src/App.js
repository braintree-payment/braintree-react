import React, { Component } from 'react'
import {Navbar, NavItem, Nav} from 'react-bootstrap'
import {  BrowserRouter as Router, Route } from 'react-router-dom'
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'
import Home from './containers/Home'
import Record from './containers/Record'

class App extends Component {
  render() {
    return (
      <LocaleProvider locale={enUS}>
        <Router>
          <div className="App">
            <Navbar>
              <Navbar.Header>
                <Navbar.Brand>
                  <a href="/">HK-01</a>
                </Navbar.Brand>
              </Navbar.Header>
              <Nav>
                <NavItem eventKey={1} href="/">Payment</NavItem>
                <NavItem eventKey={2} href="/record">Record</NavItem>
              </Nav>
            </Navbar>
            <Route exact path="/" component={Home} />
            <Route path="/record" component={Record}/>
          </div>
        </Router>
      </LocaleProvider>
    )
  }
}

export default App;
