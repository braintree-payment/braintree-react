import React, { Component } from 'react'
import { Form, Input, Button, Spin } from 'antd'
import { getPayment } from '../lib/requestHelper'
import { showModal } from '../components/showModal'
import { decryption } from '../lib/encryption'

const FormItem = Form.Item

class Record extends Component {
  state = {
    loading: false
  }

  async componentDidMount() {
    this.props.form.validateFields()
  }

  toggleLoading = (value) => {
    this.setState({ loading: value })
  }

  handleSubmit = async (e) => {
    const { toggleLoading } = this
    
    e.preventDefault()
    this.props.form.validateFields(async (err, values) => {
      let {q} = values
      if (!err && q) {
        toggleLoading(true)
      
        try {
          values.username = 'tester'
          values.q = q.replace(/\s/g,'')
          let res = await getPayment(values)
          res.data = decryption(res.data)
          showModal(res)
          toggleLoading(false)
        } catch (err) {
          toggleLoading(false)
          showModal(err)
        }

      }
    })
    
  }

  render() {
    const { getFieldDecorator, getFieldError, isFieldTouched } = this.props.form
    const { loading } = this.state
    const qError = isFieldTouched('q') && getFieldError('q')
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    }
    return (
      <Form onSubmit={this.handleSubmit}>
        <Spin spinning={loading} size="large">
          <h2>Search Record</h2>
          <FormItem
            {...formItemLayout}
            label="Search"
            validateStatus={qError ? 'error' : ''}
            help={qError || ''}
            hasFeedback>
            {getFieldDecorator('q', {
              rules: [{ required: false, message: null }]
            })(
              <Input
                className="form-control"
                placeholder="by payment ID"
                maxLength="40"
              />
              )}
          </FormItem>
          <FormItem className="div--btn">
            <Button
              type="primary"
              className="btn-primary"
              htmlType="submit">
              SEARCH
          </Button>
          </FormItem>
        </Spin>
      </Form>
    )
  }
}
export default Form.create()(Record)