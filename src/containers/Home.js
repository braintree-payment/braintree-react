import React, { Component } from 'react'
import { Form, Input, Button, DatePicker, Select, Modal, Spin } from 'antd'
import moment from 'moment'
import braintree from 'braintree-web'
import paypal from 'paypal-checkout'
import InputMask from 'react-text-mask'
import { getClientToken, checkout, getPaypalPaymentID, executePaypalPayment } from '../lib/requestHelper'
import { cardValidator } from '../lib/creditCardValidation'
import GetWindowParams from '../lib/getWindowParams'
import {disabledDate} from '../lib/antd'
import {showModal} from '../components/showModal'
import './Home.css'

const FormItem = Form.Item
const Option = Select.Option
const MonthPicker = DatePicker.MonthPicker
let defaultForm = {
  name: 'tester_01',
  phone: '23456789',
  price: 0.01,
  CCHN: 'tester_01',
  CCNum: '4111-1111-1111-1111', // 3714-4963-5398-431
  CCV: '123',
}
let defaultRules = {
  CCNumMinLength: 17,
  currency: 'HKD',
  expiryDate: moment('2017-12', 'YYYY-MM'),
  CCVMinLength: 3,
}
class Home extends Component {
  state = {
    showBox: false,
    loading: false,
  }

  componentWillMount() {
    this.toggleLoading(true)
    this.executePaypalPayment()
  }

  async componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields()
  }

  toggleLoading = (value) => {
    this.setState({ loading: value })
  }

  handleOnKeyPress = (e) => {
    if (e.charCode < 45 || e.charCode > 57) {
      return e.preventDefault()
    }
  }

  onCCChange = (e) => {
    let cardNumber = e.target.value.replace(/-/g, '')
    const isAE = cardValidator(cardNumber) === 'american-express'
    console.log('isAE', isAE)
  }

  errorMsg = (title, content) => {
    return {
      name: title,
      response: {
        data: {
          message: content
        }
      }
    }
  }

  executePaypalPayment = async (values) => {
    const { errorMsg, toggleLoading } = this
    const payerId = GetWindowParams('PayerID')
    const token = GetWindowParams('token')
    const paymentId = GetWindowParams('paymentId')
    if (payerId && token && paymentId) {
      try {
        const res = await executePaypalPayment(paymentId, payerId)
        toggleLoading(false)
        showModal(res)
      } catch (err) {
        toggleLoading(false)
        showModal(err)
      }
    }
    window.history.pushState({}, document.title, "/")
    toggleLoading(false)
  }

  createPaypalPayment = async (values) => {
    const { price, currency } = values
    const { toggleLoading } = this
    // delete card info
    delete values.CCV
    delete values.CCHN
    delete values.CCNum
    delete values.expiryDate
    let body = {
      "transactions": [
        {
          "amount": {
            "total": price,
            "currency": currency
          },
          "description": JSON.stringify(values)
        }
      ]
    }
    try {
      const res = await getPaypalPaymentID(body)
      
      res.data.links.forEach(function (link) {
        const { rel } = link
        if (rel === 'approval_url'){
          window.location = link.href
        }
      })
    } catch (err) {
      toggleLoading(false)
      showModal(err)
    }
  }

  braintreePay = async (values) => {
    const cardNumber = values.CCNum.replace(/-/g, '')
    const {toggleLoading} = this
    try {
      const client_token = await getClientToken()
      const clientInstance = await braintree.client.create({
        authorization: client_token.data
      })
      let request_nonce
      let data = {
        creditCard: {
          cardholderName: values.CCHN,
          number: cardNumber,
          expirationDate: values.expiryDate.format('MM/YY'),
          cvv: values.CCV
        }
      }

      console.log('Received values of form: ', data)

      request_nonce = await clientInstance.request({
        endpoint: 'payment_methods/credit_cards',
        method: 'post',
        data: data
      })

      values.payment_method_nonce = request_nonce.creditCards[0].nonce
      const paymentRes = await checkout(values)
      this.resetForm()
      toggleLoading(false)
      return paymentRes

    } catch (err) {
      toggleLoading(false)
      return err
    }

  }

  resetForm () {
    const {setFieldsValue} = this.props.form
    setFieldsValue({
      name: null,
      phone: null,
      price: null,
      CCHN: null,
      CCNum: null,
      CCV: null,
    })
  }
  
  // check conditions and route to different payment gateways
  handleSubmit = async (e) => {
    e.preventDefault()
    const { createPaypalPayment, toggleLoading } = this
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const cardNumber = values.CCNum.replace(/-/g, '')
        const isAE = cardValidator(cardNumber) === 'american-express'
        const { currency } = values
        if (isAE) {
          if (currency === 'USD') {
            toggleLoading(true)
            return createPaypalPayment(values)
          } else {
            showModal(this.errorMsg('Error', 'AMEX is possible to use only for USD'))
          }
        } else {
          if (currency === 'USD' || currency === 'EUR' || currency === 'AUD' || currency === 'CNY') {
            toggleLoading(true)
            return createPaypalPayment(values)
          } else {
            toggleLoading(true)
            const res = await this.braintreePay(values)
            showModal(res)
          }
        }
      } else {
        console.log(err)
        showModal({
          name: 'Opsss',
          message: 'Please fill in the *fields' 
        })
      }
    })
  }

  render() {
    const { getFieldDecorator, getFieldError, isFieldTouched } = this.props.form
    const { loading } = this.state
    const {name, phone, price, CCHN, CCNum, CCV} = defaultForm
    const {CCNumMinLength, currency, expiryDate, CCVMinLength} = defaultRules
    const nameError = isFieldTouched('name') && getFieldError('name')
    const phoneError = isFieldTouched('phone') && getFieldError('phone')
    const currencyError = isFieldTouched('currency') && getFieldError('currency')
    const priceError = isFieldTouched('price') && getFieldError('price')
    const CCHNError = isFieldTouched('CCHN') && getFieldError('CCHN')
    const CCNumError = isFieldTouched('CCNum') && getFieldError('CCNum')
    const expiryDateError = isFieldTouched('expiryDate') && getFieldError('expiryDate')
    const CCVError = isFieldTouched('CCV') && getFieldError('CCV')

    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '852',
    })(
      <Select style={{ width: 60 }}>
        <Option value="852">+852</Option>
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
      </Select>
      )
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 14,
          offset: 6,
        },
      },
    }

    return (

      <Form onSubmit={this.handleSubmit}>
        <Spin spinning={loading} size="large">
          <h2>Order</h2>
        
          <FormItem
            {...formItemLayout}
            label="NAME"
            validateStatus={nameError ? 'error' : ''}
            help={nameError || ''}
            hasFeedback>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name' }],
              initialValue: name
            })(
              <Input
                className="form-control"
                placeholder="Name"
                maxLength="40"
              />
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="PHONE"
            validateStatus={phoneError ? 'error' : ''}
            help={phoneError || ''}
            hasFeedback>
            {getFieldDecorator('phone', {
              rules: [{ required: true, message: 'Please input your phone number' }],
              initialValue: phone
            })(
              <Input addonBefore={prefixSelector} style={{ width: '100%' }} onKeyPress={this.handleOnKeyPress} maxLength="20" />
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="CURRENCY"
            validateStatus={currencyError ? 'error' : ''}
            help={currencyError || ''}
            hasFeedback>
            {getFieldDecorator('currency', {
              rules: [
                { required: true, message: 'Please select your currency' },
              ],
              initialValue: currency
            })(
              <Select placeholder="currency">
                <Option value="HKD">HKD</Option>
                <Option value="USD">USD</Option>
                <Option value="AUD">AUD</Option>
                <Option value="EUR">EUR</Option>
                <Option value="JRP">JRP</Option>
              </Select>
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="Price"
            validateStatus={priceError ? 'error' : ''}
            help={priceError || ''}
            hasFeedback>
            {getFieldDecorator('price', {
              rules: [{ required: true, message: 'Please input price' }],
              initialValue: price
            })(
              <Input style={{ width: '100%' }} onKeyPress={this.handleOnKeyPress} maxLength="60" />
              )}
          </FormItem>
              
          <h2>Payment</h2>

          <FormItem
            {...formItemLayout}
            label="CARD HOLDER NAME"
            validateStatus={CCHNError ? 'error' : ''}
            help={CCHNError || ''}
            hasFeedback>
            {getFieldDecorator('CCHN', {
              rules: [{ required: true, message: 'Please input your card holder name' }],
              initialValue: CCHN
            })(
              <Input
                className="form-control"
                placeholder="Card holder name"
                maxLength="40"
              />
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="CARD NUMBER"
            validateStatus={CCNumError ? 'error' : ''}
            help={CCNumError || ''}
            hasFeedback>
            {getFieldDecorator('CCNum', {
              rules: [{ 
                required: true, message: 'Please input your credit card number'
              },{
                min: CCNumMinLength, message: `Please input at least 14 card number`
              }],
              initialValue: CCNum
            })(
              <InputMask
                mask={[/[1-9]/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                className="form-control"
                placeholder="XXXX-XXXX-XXXX-XXXX"
                guide={false}
                onChange={(e) => this.onCCChange(e)}
              />
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="EXPIRATION DATE"
            validateStatus={expiryDateError ? 'error' : ''}
            help={expiryDateError || ''}
            hasFeedback>
            {getFieldDecorator('expiryDate', {
              rules: [{ required: true, message: 'Please input your credit card expiration date' }],
              initialValue: expiryDate
            })(
              <MonthPicker disabledDate={disabledDate} />
              )}
          </FormItem>

          <FormItem
            {...formItemLayout}
            label="CCV"
            validateStatus={CCVError ? 'error' : ''}
            help={CCVError || ''}
            hasFeedback>
            {getFieldDecorator('CCV', {
              rules: [{ 
                required: true, message: 'Please input your CCV',
                min: CCVMinLength, message: `Please input at least 3 CCV number`
              }],
              initialValue: CCV
            })(
              <InputMask
                mask={[/[1-9]/, /\d/, /\d/, /\d/]}
                className="form-control"
                placeholder="123"
                guide={false}
              />
              )}
          </FormItem>

          <FormItem className="div--btn">
            <Button
              type="primary"
              className="btn--primary"
              htmlType="submit">
              PAY
          </Button>
          </FormItem>
        </Spin>
      </Form>

    )
  }
}
export default Form.create()(Home)