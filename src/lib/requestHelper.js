import axios from 'axios'
import config from '../config'

const doRequest = (method, url, data, headers) => {
  return axios({
    method: method,
    url: url,
    data: data,
    headers: headers
  }).then(res => res, err => {
    throw err
  })
}

export const getClientToken = () => {
  return doRequest('GET', `${config.baseUrl}/payment/braintree/client-token`)
}

export const checkout = (values) => {
  return doRequest('POST', `${config.baseUrl}/payment/braintree/checkout`, values)
}

export const getPaypalPaymentID = (transactions) => {
  return doRequest('POST', `${config.baseUrl}/payment/paypal/create-payment`, transactions)
}

export const executePaypalPayment = (payment_id, payer_id) => {
  let data = {
    payer_id: payer_id
  }
  return doRequest('POST', `${config.baseUrl}/payment/paypal/execute/${payment_id}`, data)
}

export const getPayment = (q) => {
  let data = {
    username: q.username
  }
  return doRequest('GET', `${config.baseUrl}/payment/${q.q}`)
}