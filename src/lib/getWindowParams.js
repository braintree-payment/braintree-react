export default (query) => {
  let searchParams = new URLSearchParams(window.location.search)
  return searchParams.get(query)
}