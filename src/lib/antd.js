export const disabledDate = (current) => {
  // Can not select days before today and today
  return current && current.valueOf() < Date.now();
}
