import valid from 'card-validator'

export const cardValidator =  (creditCardNumber) => {
  let numberValidation = valid.number(creditCardNumber)
  if (!numberValidation.isPotentiallyValid){
    return null
  }
  if (numberValidation.card){
    return numberValidation.card.type
  }
}