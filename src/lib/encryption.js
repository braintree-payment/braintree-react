import CryptoJS from 'crypto-js'
import {ENCRYPT_KEY} from '../config'

export const encryption = (data) => {
  let ciphertext
  if (typeof data === 'string'){
    ciphertext = CryptoJS.AES.encrypt(data, ENCRYPT_KEY)
    return ciphertext.toString()
  } else {
    ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), ENCRYPT_KEY)
    return ciphertext.toString()
  }
}

export const decryption = (data) => {
  let bytes  = CryptoJS.AES.decrypt(data, ENCRYPT_KEY)
  let decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  return decryptedData
}